from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from book.models import booking

# Create your views here.
def history(request):
	daftar_parkir = {"daftar_parkir":booking.objects.all().values()}
	return render(request,'hist.html',daftar_parkir)