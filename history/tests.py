from django.test import TestCase,Client
from django.urls import resolve
from .views import*

# Create your tests here.
class test_Unit(TestCase):
 	def test_history_url_is_exist(self):
 		response = Client().get('/history/')
 		self.assertEqual(response.status_code,200)
	
 	def test_history_template_exist(self):
 		response = Client().get('/history/')
 		self.assertTemplateUsed(response,'hist.html')

 	def test_history_file_render(self):
 		found = resolve('/history/')
 		self.assertEqual(found.func,history)
		