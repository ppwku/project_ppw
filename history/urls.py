from django.urls import path,include
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

app_name='history'

urlpatterns = [
    path('', views.history, name='history'),   
]

urlpatterns += staticfiles_urlpatterns()
