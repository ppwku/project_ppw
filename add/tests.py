from django.test import TestCase, Client
from django.urls import resolve
from .models import daftar
from .views import *

# Create your tests here.
class test_Unit(TestCase):
    def test_add_url_is_exist(self):
        response= Client().get('/add/')
        self.assertEqual(response.status_code,200)

    def test_template_exist(self):
        response= Client().get('/add/')
        self.assertTemplateUsed(response,"dus.html")
        
    def test_file_render(self):
        found=resolve("/add/")
        self.assertEqual(found.func,get_schedule)

    def test_model(self):
        new_daftar=daftar.objects.create(
            display_nama_tempat_parkir='cigombong',
            display_lokasi='jawa barat'
        )
        jumlah=daftar.objects.all().count()
        self.assertEqual(jumlah,1)


    