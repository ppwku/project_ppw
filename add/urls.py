from django.urls import path,include
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

app_name='add'

urlpatterns = [
    path('', views.get_schedule, name='adds'),
    
]

urlpatterns += staticfiles_urlpatterns()
