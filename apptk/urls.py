from django.urls import path
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

app_name='apptk'

urlpatterns = [
    path('', views.signin, name='signin'),
    path('hello/', views.home, name='home'),
    
]

urlpatterns += staticfiles_urlpatterns()
