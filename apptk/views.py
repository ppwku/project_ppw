from django.shortcuts import render

def home(request):
    return render(request,'hello.html')

def signin(request):
    return render(request,'signin.html')
    