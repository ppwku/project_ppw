from django.test import TestCase, Client
from django.urls import resolve
from .views import home,signin

# Create your tests here.
class test_Unit(TestCase):
    def test_add_url_is_exist(self):
        response= Client().get('/')
        self.assertEqual(response.status_code,200)

        
    def test_template_exist(self):
        response= Client().get('/hello/')
        self.assertTemplateUsed(response,"hello.html")

    def test_template2_exist(self):
        response= Client().get('')
        self.assertTemplateUsed(response,"signin.html")
        
    def test_file_render(self):
        found=resolve("/hello/")
        self.assertEqual(found.func,home)

    def test_file2_render(self):
        found=resolve("/")
        self.assertEqual(found.func,signin)