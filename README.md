-Anggota Kelompok:

1)Kurnia Ilham Wijayadi
2)Nicholas Pangestu
3)Setyawan Pratama



-Cerita tentang aplikasi:

Tiap hari seseorang pasti bepergian, baik itu untuk kerja ataupun bermain. Transportasi merupakan sarana untuk membantu terwujudnya tujuan ini. Jika kita perhatikan,
banyak orang yang tiap harinya bepergian menggunakan transportasi pribadi maupun umum untuk sampai ke tujuan mereka. Tidak jarang juga ada yang menggunakan transportasi pribadinya 
untuk pergi dahulu ke tempat penyedia jasa transportasi umum seperti bandara,stasiun,pelabuhan,dll. Transportasi umum dinilai lebih murah dan lebih nyaman dipilih oleh kebanyakan orang 
yang bepergian dari kota ke kota lain. 

Parkir merupakan salah satu hal penting yang perlu diperhatikan apabila dalam bepergian menggunakan transportasi pribadi. Tentunya tiap orang mau agar saat mereka menjalankan kegiatannya,
mereka tidak merasa cemas akan keadaan dari kendaraan mereka. Kebanyakan parkiran yang baik dipilih karena adanya jaminan serta kualitas keamanan yang terbukti baik. Contoh parkiran yang
seperti itu biasanya terdapat pada tempat-tempat umum. Salah satu tempat parkiran yang selalu dikunjungi oleh banyak orang adalah stasiun. Kebanyakan dari pengguna parkiran pada stasiun
adalah orang-orang yang bekerja tiap harinya. Terbatasnya ruang yang tersedia pada parkiran di stasiun membuat banyak orang harus mencari tempat parkiran lain. Masalah yang dialami oleh
kebanyakan orang adalah ketika sampai di stasiun dan ingin memarkirkan kendaraan mereka, seluruh ruang parkir sudah terisi penuh, sehingga mereka harus mencari tempat parkir lain. 
Tentunya hal ini dapat merugikan banyak orang, karena akan memakan waktu lebih banyak untuk mencari parkiran lain. 

Disini, kami hadir dengan menciptakan sebuah website yang dapat menjadi solusi dari permasalahan diatas yaitu masalah parkir pada stasiun. Produk ini kami beri nama parkingin.
Produk ini kami ciptakan agar banyak orang dapat mengetahui keadaan parkir di suatu stasiun dan memesan slot parkir bahkan sebelum mereka berangkat. Kami berharap agar para pengguna
produk kami bisa mendapatkan kepastian dan dapat menentukan pilihan terbaik mereka pada saat bepergian ke stasiun. Produk ini dapat digunakan oleh siapa saja, dan dimanfaatkan untuk
berbagai keperluan yang berhubungan dengan kegiatan parkir. Harapan kami adalah agar parkir tidak lagi menjadi masalah yang perlu dikhawatirkan oleh orang-orang.

-Link Heroku:http://parkingin.herokuapp.com/

-Fitur-fitur yang akan diimplementasikan(saat ini):
1)Booking parkir(utama)
2)Riwayat booking
3)Memberi usul untuk menambahkan stasiun yang belum tercakup

status pipeline:
[![pipeline status](https://gitlab.com/ppwku/project_ppw/badges/master/pipeline.svg)](https://gitlab.com/ppwku/project_ppw/commits/master)
[![coverage report](https://gitlab.com/ppwku/project_ppw/badges/master/coverage.svg)](https://gitlab.com/ppwku/project_ppw/commits/master)


