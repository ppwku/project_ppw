from django.urls import path
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

app_name='book'

urlpatterns = [
    path('', views.book, name='book'),
    
]

urlpatterns += staticfiles_urlpatterns()
